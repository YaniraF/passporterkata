type Direction = "N" | "E" | "S" | "O";
type Command = "f" | "b" | "r" | "l";
type Coordinates = { x: number; y: number };

export class MarsRover {
  position: Coordinates = { x: 0, y: 0 };
  direction: Direction = "N";

  coordinates() {
    return this.position;
  }

  facing() {
    return this.direction;
  }

  move(commands: Command[]) {
    commands.forEach((command: Command) => {
      if (command === "f") this.advance();
      else if (command === "b") this.retreat();
      else if (command === "r") this.turnRight();
      else if (command === "l") this.turnLeft();
    });
  }

  advance() {
    const coordinatesToAdd: Record<Direction, Coordinates> = {
      N: { x: 0, y: 1 },
      E: { x: 1, y: 0 },
      S: { x: 0, y: -1 },
      O: { x: -1, y: 0 },
    };

    this.position.x = this.position.x + coordinatesToAdd[this.direction].x;
    this.position.y = this.position.y + coordinatesToAdd[this.direction].y;
  }

  retreat() {
    const coordinatesToAdd: Record<Direction, Coordinates> = {
      N: { x: 0, y: -1 },
      E: { x: -1, y: 0 },
      S: { x: 0, y: 1 },
      O: { x: 1, y: 0 },
    };

    this.position.x = this.position.x + coordinatesToAdd[this.direction].x;
    this.position.y = this.position.y + coordinatesToAdd[this.direction].y;
  }

  turnRight() {
    const fromTo: Record<Direction, Direction> = {
      N: "E",
      E: "S",
      S: "O",
      O: "N",
    };

    this.direction = fromTo[this.direction];
  }

  turnLeft() {
    const fromTo: Record<Direction, Direction> = {
      N: "O",
      E: "N",
      S: "E",
      O: "S",
    };

    this.direction = fromTo[this.direction];
  }
}
