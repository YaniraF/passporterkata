import { expect } from "@jest/globals";
import { MarsRover } from "./some_kata";

describe("Some kata", () => {
  it("starts at 0, 0 coordinates", () => {
    const rover = new MarsRover();
    const position = rover.coordinates();
    const initialPosition = { x: 0, y: 0 };

    expect(position).toStrictEqual(initialPosition);
  });

  it("starts facing North", () => {
    const rover = new MarsRover();
    const position = rover.facing();
    const initialDirection = "N";

    expect(position).toBe(initialDirection);
  });

  it("moves forward", () => {
    const rover = new MarsRover();
    const expectedPosition = { x: 0, y: 1 };

    rover.move(["f"]);
    const position = rover.coordinates();

    expect(position).toStrictEqual(expectedPosition);
  });

  it("moves forward twice", () => {
    const rover = new MarsRover();
    const expectedPosition = { x: 0, y: 2 };

    rover.move(["f", "f"]);
    const position = rover.coordinates();

    expect(position).toStrictEqual(expectedPosition);
  });

  it("moves backwards", () => {
    const rover = new MarsRover();
    const expectedPosition = { x: 0, y: -1 };

    rover.move(["b"]);
    const position = rover.coordinates();

    expect(position).toStrictEqual(expectedPosition);
  });

  it("turns right", () => {
    const rover = new MarsRover();
    const expectedDirection = "E";

    rover.move(["r"]);
    const position = rover.facing();

    expect(position).toStrictEqual(expectedDirection);
  });

  it("turns right twice", () => {
    const rover = new MarsRover();
    const expectedDirection = "S";

    rover.move(["r", "r"]);
    const position = rover.facing();

    expect(position).toStrictEqual(expectedDirection);
  });

  it("turns left", () => {
    const rover = new MarsRover();
    const expectedDirection = "O";

    rover.move(["l"]);
    const position = rover.facing();

    expect(position).toStrictEqual(expectedDirection);
  });

  it("moves freely", () => {
    const rover = new MarsRover();
    const expectedDirection = "O";
    const expectedPosition = { x: -1, y: -1 };

    rover.move(["l", "f", "f", "l", "f", "r", "b"]);
    const position = rover.coordinates();
    const direction = rover.facing();

    expect(position).toStrictEqual(expectedPosition);
    expect(direction).toBe(expectedDirection);
  });
});
